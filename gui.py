from game import *
from screen import *
from button import *

BACKGROUND = (128, 86, 61)
FONT_COLOR = (235, 166, 38)
PLAY_COLOR = (94, 181, 106)
PLAY_HOVER = (42, 82, 48)
HELP_COLOR = (186, 58, 184)
HELP_HOVER = (74, 23, 73)
TITLE_COLOR = (255, 255, 255)
TITLE_BORDER_COLOR = (194, 23, 23)
TITLE_BACK_COLOR = (28, 99, 12)


pygame.init()
pygame.font.init()

start_screen = Screen("Clue Start Screen", BACKGROUND, 1000, 880)
play_screen = Screen("Clue Board", BACKGROUND, 1000, 880)
help_screen = Screen("Clue Help", BACKGROUND, 1000, 880)

start_screen.make_current()

# toggle = False


class Board(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load("images/board.jpg")
        self.rect = self.image.get_rect()
        self.rect.center=(355,375)

    def draw(self, surface):
        surface.blit(self.image, self.rect)


class Help(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load("images/rsz_help_screens.jpg")
        self.rect = self.image.get_rect()
        self.rect.center = (500,375)

    def draw(self, surface):
        surface.blit(self.image, self.rect)


BOARD1 = Board()
HELP = Help()
game_title_font = pygame.font.SysFont('malgungothic', 40, True, False)

done = False

first_loop = True

play_button = Button(350, 400, 300, 100, PLAY_COLOR, PLAY_HOVER, "ariel", 40, FONT_COLOR, "Play")
help_button = Button(350, 600, 300, 100, HELP_COLOR, HELP_HOVER, "ariel", 40, FONT_COLOR, "Help")

game = Game()

show = True
while not done:
    start_screen.screen_update()
    play_screen.screen_update()
    help_screen.screen_update()
    mouse_coord = pygame.mouse.get_pos()
    mouse_press = pygame.mouse.get_pressed()
    # keys = pygame.key.get_pressed()

    if start_screen.check_current():
        game.set_surface(start_screen.return_screen())
        title_text = "Welcome to the game of Clue"
        pygame.draw.rect(start_screen.return_screen(), TITLE_BORDER_COLOR, [180, 160, 640, 150])
        pygame.draw.rect(start_screen.return_screen(), TITLE_BACK_COLOR, [190, 170, 620, 130])
        title = game_title_font.render(title_text, True, TITLE_COLOR)
        start_screen.return_screen().blit(title, [210, 200])
        play = play_button.mouse_check(mouse_coord, mouse_press)
        play_button.show_button(start_screen.return_screen())
        help = help_button.mouse_check(mouse_coord, mouse_press)
        help_button.show_button(start_screen.return_screen())

        if play:
            win = play_screen.make_current()
            start_screen.end_current()

        elif help:
            win = help_screen.make_current()
            start_screen.end_current()

    if play_screen.check_current():
        # display board and user cards
        BOARD1.draw(play_screen.return_screen())
        # can put next two lines into a for loop to iterate through a deck of cards
        game.draw_user_cards()

        game.set_surface(play_screen.return_screen())

        if not game.game_over and not first_loop:
            game.take_turn()
        first_loop = False
        if game.game_over:
            sys.exit(0)

    # Sean - help screen displays help_screen image (image may need updating)
    if help_screen.check_current():
        HELP.draw(help_screen.return_screen())

        game.set_surface(help_screen.return_screen())

        # add play button
        play_button.change_xy(350, 700)
        play = play_button.mouse_check(mouse_coord, mouse_press)
        play_button.show_button(start_screen.return_screen())
        if play:
            win = play_screen.make_current()
            help_screen.end_current()

    for event in pygame.event.get():
        if(event.type == pygame.QUIT):
            done = True

    pygame.display.update()

pygame.quit()
