# Clue Final Project
## How to Play
This is the same clue game that you know and (maybe) love.
You will play as Miss Scarlet represented by a red square against
two computers Mr. Green (green square) and Mrs. Peacock (light blue square).

If you need to review the rules you can simply click the help button in-game.
## Controls
When your player is not in a room you can use the arrow keys to
move up/down and left/right as you would expect.
To open your notes press `N`.

The rest of the game is controlled by other windows that you will need to click through
to make your decisions. Simply follow the prompt presented and enjoy the game.

## Note to Grader
To run just run `gui.py`. For testing winning a game see line 149 in `game.py`
See line 172 in `game.py` to test movement easily and check that all moves will be valid.

The green and blue squares are the computer player. They are awful at pathfinding as
Jason said they didn't need to be good; however if it looks like they aren't doing
anything they are but they just aren't very good at the game.